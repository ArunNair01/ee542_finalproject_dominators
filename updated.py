#!/usr/bin/env python
# coding: utf-8

# In[9]:


# import libraries
import boto3, re, sys, math, json, os, sagemaker, urllib.request
from sagemaker import get_execution_role
import numpy as np                                
import pandas as pd                               
import matplotlib.pyplot as plt                   
from IPython.display import Image                 
from IPython.display import display               
from time import gmtime, strftime                 
from sagemaker.predictor import csv_serializer   
from sagemaker.amazon.amazon_estimator import get_image_uri

# Define IAM role
role = get_execution_role()
prefix = 'finalproject'

my_region = boto3.session.Session().region_name # set the region of the instance


# In[195]:


bucket='dominatorsfinalproject'
data_key = 'try_full_label4.csv'
data_location = 's3://{}/{}'.format(bucket, data_key)


# In[196]:


import hashlib
import os 
import logging
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import pdb

from sklearn import datasets
from sklearn.metrics import roc_curve,auc
from scipy import interp
from itertools import cycle
import time

from sklearn.manifold import TSNE
from sklearn.feature_selection import SelectFromModel
from sklearn import datasets
from sklearn.linear_model import LassoCV
from sklearn.linear_model import MultiTaskLassoCV
from sklearn.linear_model import Lasso
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA


# In[197]:


label_list=['No cancer','Kidney cancer' ,'Thyroid cancer' ,'Blood cancer','Uterus cancer' ,'Head and Neck cancer','Ovary cancer' ,'Breast cancer','Liver cancer','Esophagus cancer' ,'Brain cancer','Cervix cancer' ,'Lung cancer' ,'Bladder cancer','Colorectal cancer','Stomach cancer','Eye cancer','Pancreas cancer' ,'Adrenal Gland cancer','Prostate cancer' ,'Soft Tissue cancer','Testis cancer' ,'Skin cancer','Bone Marrow cancer' ,'Lymph Nodes cancer' ,'Pleura cancer','Thymus cancer','Bile Duct cancer']
def lassoSelection(X_train, y_train, n):
    
    '''
    Lasso feature selection.  Select n features. 
    '''
    #lasso feature selection
    #print (X_train)
    clf = LassoCV()
    sfm = SelectFromModel(clf, threshold=0)
    sfm.fit(X_train, y_train)
    X_transform = sfm.transform(X_train)
    n_features = X_transform.shape[1]
    
    #print(n_features)
    while n_features > n:
        sfm.threshold += 0.01
        X_transform = sfm.transform(X_train)
        n_features = X_transform.shape[1]
    features = [index for index,value in enumerate(sfm.get_support()) if value == True  ]
    logging.info("selected features are {}".format(features))
    return features


# In[198]:


def specificity_score(TN,FP):
    TNR = TN/(TN+FP)
    return TNR


# In[199]:


def model_fit_predict(X_train,X_test,y_train,y_test):

        np.random.seed(2018)
        
        from sklearn.tree import DecisionTreeClassifier
        from sklearn.metrics import precision_score
        from sklearn.metrics import accuracy_score
        from sklearn.metrics import f1_score
        from sklearn.metrics import recall_score
        from sklearn.metrics import confusion_matrix
        
        models = {'DecisionTreeClassifier':DecisionTreeClassifier()}
        tuned_parameters = {'DecisionTreeClassifier': {'max_depth':[30]} }
        scores= {}
        #for key in models:
       
        clf = GridSearchCV(models['DecisionTreeClassifier'], tuned_parameters['DecisionTreeClassifier'],scoring=None,  refit=True, cv=10)
        clf.fit(X_train,y_train)
        y_test_predict = clf.predict(X_test)
        cnf_matrix = confusion_matrix(y_test, y_test_predict)
        FP = cnf_matrix.sum(axis=0) - np.diag(cnf_matrix)  
        FN = cnf_matrix.sum(axis=1) - np.diag(cnf_matrix)
        TP = np.diag(cnf_matrix)
        TN = cnf_matrix.sum() - (FP + FN + TP)
        print(y_test)
        print(y_test_predict)
        FP = FP.astype(float)
        FN = FN.astype(float)
        TP = TP.astype(float)
        TN = TN.astype(float)

        TPR = TP/(TP+FN)
        TNR = TN/(TN+FP)
        FPR = FP/(FP+TN)
        FNR = FN/(TP+FN)

        PPV = TP/(TP+FP)
        ACC = (TP+TN)/(TP+FP+FN+TN)
        recall = TP/(TP+FN)
        #print(PPV,ACC,recall)
        #pdb.set_trace()
        #plot_roc(y_test_predict,y_test)
        #pdb.set_trace()
        precision = precision_score(y_test, y_test_predict,average=None)
        #accuracy = accuracy_score(y_test, y_test_predict)
        accuracy=ACC
        f1 = f1_score(y_test, y_test_predict,average=None)
        recall = recall_score(y_test, y_test_predict,average=None)
        #specificity = specificity_score(y_test, y_test_predict)
        specificity = specificity_score(TN,FP)
        scores['DecisionTreeClassifier'] = [precision,accuracy,f1,recall,specificity]
        #print(cnf_matrix)
        #print(scores)
        #pdb.set_trace()

        return (scores,y_test_predict)


# In[200]:


def draw(scores):
        '''
        draw scores.
        '''
        import matplotlib.pyplot as plt
        logging.info("scores are {}".format(scores))
        fig = plt.figure(figsize=(10,10))
        
        
        precisions = []
        accuracies =[]
        f1_scores = []
        recalls = []
        categories = []
        specificities = []
        N = len(scores)
        ind = np.arange(N)  # set the x locations for the groups
        width = 0.01        # the width of the bars
        precisions.append(scores['DecisionTreeClassifier'][0])
        accuracies.append(scores['DecisionTreeClassifier'][1])
        f1_scores.append(scores['DecisionTreeClassifier'][2])
        recalls.append(scores['DecisionTreeClassifier'][3])
        specificities.append(scores['DecisionTreeClassifier'][4])
        i=0
        j=1
        #print(precisions)
        #print(accuracies)
        #print(f1_scores)
        #print(recalls)
        while(i<27):
            if(i==4):
                    ax = plt.subplot(1,1,1)
                    precision_bar = ax.bar(ind, precisions[0][i],width=0.01,color='b',align='center')
                    accuracy_bar = ax.bar(ind+1*width, accuracies[0][i],width=0.01,color='g',align='center')
                    f1_bar = ax.bar(ind+2*width, f1_scores[0][i],width=0.01,color='r',align='center')
                    recall_bar = ax.bar(ind+3*width, recalls[0][i],width=0.01,color='y',align='center')
                    specificity_bar = ax.bar(ind+4*width,specificities[0][i],width=0.01,color='purple',align='center')
                    ax.set_xlabel(label_list[i])
                    break
            else:
                    i+=1
                    j+=1
               
        #plt.grid()
        #ax=plt.subplot(6,6,36)
        fig.legend((precision_bar, accuracy_bar,f1_bar,recall_bar,specificity_bar), ('precision', 'accuracy','f1','sensitivity','specificity'),loc='upper right')
        plt.draw()


# In[201]:


df=pd.read_csv(data_location)
df1=df.copy()      
y_data=df.pop('label').values
#df.pop('file_id')
columns =df.columns
#print (columns)
X_data = df.values
#df1.pop('file_id')

# split the data to train and test set
X_train, X_test, y_train, y_test = train_test_split(X_data, y_data, test_size=0.4, random_state=20)
               
#standardize the data.
scaler = StandardScaler()
scaler.fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)
       
n = 50
feaures_columns = lassoSelection(X_train, y_train, n)


# In[ ]:


scores,y_test_predict = model_fit_predict(X_train[:,feaures_columns],X_test[:,feaures_columns],y_train,y_test)


# In[203]:


draw(scores)


# In[204]:


def plot_corr_heatmap(x_train, y_train):
    x_train = pd.DataFrame(x_train)
    label = pd.DataFrame(data = y_train, columns = ['label'])
    data = x_train.join(label)
    data=data.head(20)
    #data=pd.concat([x_train,label],axis=1)
    #print(data)
    corr = data.corr()
    #print (corr)
    plt.figure(figsize = (10,10))
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    g = sns.heatmap(corr, xticklabels=corr.columns.values,
                    yticklabels=corr.columns.values, cmap=cmap,
                    vmax=.3, center=0, square=True, linewidths=.1,
                    cbar_kws={"shrink": .82})
    g.set_xticklabels(g.get_xticklabels(), rotation=90)
    g.set_yticklabels(g.get_yticklabels(), rotation=0)
    plt.title('Heatmap of Correlation Matrix')
    
    # Extract the 'y' column (the dependent variable)
    corr_deposit = pd.DataFrame(corr['label'].drop('label'))
    corr_deposit = corr_deposit.sort_values(by = 'label', ascending = False)
    
    plt.show()


# In[205]:


import seaborn as sns
plot_corr_heatmap(X_train[:,feaures_columns],y_train)


# In[206]:


print("precision=",scores['DecisionTreeClassifier'][0][4])
print("accuracy=",scores['DecisionTreeClassifier'][1][4])
print("f1=",scores['DecisionTreeClassifier'][2][4])
print("recall=",scores['DecisionTreeClassifier'][3][4])
print("specificity=",scores['DecisionTreeClassifier'][4][4])


# In[ ]:


print(len(feaures_columns))


# In[207]:


feaures_columns.pop(39)


# In[143]:


feaures_columns.append(1237)


# In[44]:


feaures_columns


# In[ ]:




