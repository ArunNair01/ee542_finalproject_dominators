# Team DOMINATORS
Members-
Shubham Ahuja
Arun Moni Nair
Manjunath Chole

Steps-

1. install all the packages needed. (sklearn, pandas , seaborn)

pip install sklearn 
pip install pandas
Pip install matplotlib
Pip install seaborn as sns



2. Source codes:

check.py:
		 This is to check the integrity for the downloaded miRNA(gene expression) files
		 "python check.py" 

parse_file_case_id.py:  
		This is to get the unique file id and the corresponding case ids.	
		"python parse_file_case_id.py"

request_meta.py: This is to request the clinical data for the files and cases.
		"python request_meta.py"

gen_miRNA_matrix.py: 
                This is to generate half of the final matrix for applying ML(half_matrix) for all the files 
		"python gen_miRNA_matrix.py"

dominators_label.py: 

                  This is to generate the labels and encode the clinical data for all the files and then merge the matrix created in previous step with this, and finally generate the miRNA matrix which is
                  is 'dominators_mat.csv'.
		  "python dominators_label.py"


baseline.py: 
                This is for getting baseline results by applying Decision Tree Machine learning algorithm to the miRNA matrix for all cancer types detection.
		"python baseline.py"

KmeansEMR.py : 

                This is to run Kmeans clustering on AWS EMR. this is to be uploaded in S3 bucket.
		"spark-submit --deploy-mode cluster --master yarn --num-executors 2 --executor-cores 2 --executor-memory 5g 'path to source code in S3' 'path to input file in txt format in S3' 'path to 
                output directory in S3'"
		example - spark-submit --deploy-mode cluster --master yarn --num-executors 2 --executor-cores 2 --executor-memory 5g 's3://dominatorsfinalproject/kMeansEMR.py' 's3://
                dominatorsfinalproject/dominators_mat.txt' 's3://dominatorsfinalproject//output'

visual_PCA_tSNE.py:
 
                This code implements the tsne and PCA method for visualizing the data ,we use this to plot the clusters obtained from k-means clustering for each cancer type to visualize how each cluster
                stands out.
                " python visual_PCA_tSNE.py file_name label_number number_of_clusters"

similarity.py :	
                After we find the correlation between cancer types and clinical data for two cancers. we do cross correlation and find the similar biomarkers using this.
		"python similarity.py"

common_biomarkers.py : 
                After we see the covergence in the Kmeans clustering. We take the biomarkers which are left(after removal of biomarkers) from the original file and create a new matrix.
		"python common_biomarkers.py"

updated.py : 
                This is for getting results by applying data analytics and tuning the parameters of the baseline model to the final matrix obtained in the previous step.
		"python updated.py"