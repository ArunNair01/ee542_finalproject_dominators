'''
This code implements the tsne and PCA method for visualizing the data ,we use this to plot the clusters obtained from 
k-means clustering for each cancer type to visualize how each cluster stands out.
'''

import pandas as pd 
import hashlib
import os 
import logging
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import numpy as np
import pdb
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.metrics import roc_curve,auc
from scipy import interp
from itertools import cycle
import time

from sklearn.manifold import TSNE
from sklearn.feature_selection import SelectFromModel
from sklearn import datasets
from sklearn.linear_model import LassoCV
from sklearn.linear_model import MultiTaskLassoCV
from sklearn.linear_model import Lasso
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA

import seaborn as sns

import sys

labels=['No cancer','Kidney cancer' ,'Thyroid cancer' ,'Blood cancer','Uterus cancer' ,'Head and Neck cancer','Ovary cancer' ,'Breast cancer','Liver cancer','Esophagus cancer' ,'Brain cancer','Cervix cancer' ,'Lung cancer' ,'Bladder cancer','Colorectal cancer','Stomach cancer','Eye cancer','Pancreas cancer' ,'Adrenal Gland cancer','Prostate cancer' ,'Soft Tissue cancer','Testis cancer' ,'Skin cancer','Bone Marrow cancer' ,'Lymph Nodes cancer' ,'Pleura cancer','Thymus cancer','Bile Duct cancer']


##Function which implements the tsne and PCA method 
def tsne_visual(data,df1,pca=False,k=8,label=0):

    if(pca == True):
        pca_50 = PCA(n_components=50)
        pca_result_50 = pca_50.fit_transform(data)
        tsne = TSNE(n_components=2)
        tsne_result = tsne.fit_transform(pca_result_50)
        principalDf = pd.DataFrame(data = tsne_result
                 , columns = ['principal component 1', 'principal component 2'])
        finalDf = pd.concat([principalDf, df1[['clusters']]], axis = 1)
        fig = plt.figure(figsize = (8,8))
        ax = fig.add_subplot(1,1,1) 
        ax.set_xlabel('x_tsne', fontsize = 15)
        ax.set_ylabel('y_tsne', fontsize = 15)
        ax.set_title(labels[label], fontsize = 20)

        if (k ==8):##plotting 8 clusters

            label_list=['Cluster-0','Cluster-1' ,'Cluster-2','Cluster-3','Cluster-4' ,'Cluster-5','Cluster-6','Cluster-7']
            targets=[]
            for i in range(0,8):
                targets.append(i)
            colors = ['r', 'g', 'b','y','khaki','m','black','bisque']
            for target, color in zip(targets,colors):
                indicesToKeep = finalDf['clusters'] == target
                ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                           , finalDf.loc[indicesToKeep, 'principal component 2']
                           , c = color
                           , s = 50)
            ax.legend(label_list)
            ax.grid()
            #plt.show()
            plt.draw()
            plt.pause(0.001)
            input("Press [enter] to continue.")

        elif (k==4):##plots 4 clusters

            label_list=['Cluster-0','Cluster-1' ,'Cluster-2','Cluster-3']
            targets=[]
            for i in range(0,4):
                targets.append(i)
            #targets = ['Adrenal Gland cancer','Bile Duct cancer','Bladder cancer','Blood cancer','Bone Marrow cancer','Brain cancer','Breast cancer','Cervix cancer','Colorectal cancer','Esophagus cancer','Eye cancer','Head and Neck cancer','Kidney cancer','Liver cancer','Lung cancer','Lymph Nodes cancer','No cancer','Ovary cancer','Pancreas cancer','Pleura cancer','Prostate cancer','Skin cancer','Soft Tissue cancer','Stomach cancer','Testis cancer','Thymus cancer','Thyroid cancer','Uterus cancer']
            colors = ['r', 'g', 'b','y']
            for target, color in zip(targets,colors):
                indicesToKeep = finalDf['clusters'] == target
                ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                           , finalDf.loc[indicesToKeep, 'principal component 2']
                           , c = color
                           , s = 50)
            ax.legend(label_list)
            ax.grid()
            #plt.show()
            plt.draw()
            plt.pause(0.001)
            input("Press [enter] to continue.")

        elif(k==3):##plots 3 clusters

            label_list=['Cluster-0','Cluster-1' ,'Cluster-2']
            targets=[]
            for i in range(0,3):
                targets.append(i)
            colors = ['r', 'g', 'b']
            for target, color in zip(targets,colors):
                indicesToKeep = finalDf['clusters'] == target
                ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                           , finalDf.loc[indicesToKeep, 'principal component 2']
                           , c = color
                           , s = 50)
            ax.legend(label_list)
            ax.grid()
            #plt.show()
            plt.draw()
            plt.pause(0.001)
            input("Press [enter] to continue.")

        elif(k==2):##plots 2 clusters

            label_list=['Cluster-0','Cluster-1']
            targets=[]
            for i in range(0,2):
                targets.append(i)
            colors = ['r', 'g']
            for target, color in zip(targets,colors):
                indicesToKeep = finalDf['clusters'] == target
                ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                           , finalDf.loc[indicesToKeep, 'principal component 2']
                           , c = color
                           , s = 50)
            ax.legend(label_list)
            ax.grid()
            #plt.show()
            plt.draw()
            plt.pause(0.001)
            input("Press [enter] to continue.")
    else:
        
        tsne = TSNE(n_components=2)
        tsne_result = tsne.fit_transform(data)
        principalDf = pd.DataFrame(data = tsne_result
                 , columns = ['principal component 1', 'principal component 2'])
        finalDf = pd.concat([principalDf, df1[['label']]], axis = 1)
        fig = plt.figure(figsize = (8,8))
        ax = fig.add_subplot(1,1,1) 
        ax.set_xlabel('x_tsne', fontsize = 15)
        ax.set_ylabel('y_tsne', fontsize = 15)
        ax.set_title('tsne dimensions', fontsize = 20)

        label_list=['No cancer','Kidney cancer' ,'Thyroid cancer' ,'Blood cancer','Uterus cancer' ,'Head and Neck cancer','Ovary cancer' ,'Breast cancer','Liver cancer','Esophagus cancer' ,'Brain cancer','Cervix cancer' ,'Lung cancer' ,'Bladder cancer','Colorectal cancer','Stomach cancer','Eye cancer','Pancreas cancer' ,'Adrenal Gland cancer','Prostate cancer' ,'Soft Tissue cancer','Testis cancer' ,'Skin cancer','Bone Marrow cancer' ,'Lymph Nodes cancer' ,'Pleura cancer','Thymus cancer' ,'Bile Duct cancer']
        targets=[]
        for i in range(0,28):
            targets.append(i)
        #targets = ['Adrenal Gland cancer','Bile Duct cancer','Bladder cancer','Blood cancer','Bone Marrow cancer','Brain cancer','Breast cancer','Cervix cancer','Colorectal cancer','Esophagus cancer','Eye cancer','Head and Neck cancer','Kidney cancer','Liver cancer','Lung cancer','Lymph Nodes cancer','No cancer','Ovary cancer','Pancreas cancer','Pleura cancer','Prostate cancer','Skin cancer','Soft Tissue cancer','Stomach cancer','Testis cancer','Thymus cancer','Thyroid cancer','Uterus cancer']
        colors = ['r', 'g', 'b','y','khaki','m','black','bisque','maroon','indianred','darkgoldenrod','honeydew','skyblue','mintcream','slategrey','oldlace','deeppink','aqua','fuchsia','violet','gold','ivory','darksalmon','chocolate','lawngreen','tan','lime','darkcyan']
        for target, color in zip(targets,colors):
            indicesToKeep = finalDf['label'] == target
            ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
                       , finalDf.loc[indicesToKeep, 'principal component 2']
                       , c = color
                       , s = 50)
        ax.legend(label_list)
        ax.grid()
        plt.draw()
        plt.pause(0.001)
        input("Press [enter] to continue.")


                
        
if __name__ == '__main__':


        #data_dir ="/Users/Arun/Documents/USC_Work/EE542/PCA_tsne_plots/"

        data_file = sys.argv[1]   ###input data file
        labelno = int(sys.argv[2]) ##label number which corresponds to cancer type
        k_val = int(sys.argv[3])  ##Number of clusters
        
        df = pd.read_csv(data_file)
        df1=df.copy()
      
        y_data=df.pop('clusters').values
        columns =df.columns
        X_data = df.values
        tsne_visual(X_data,df1,pca=True,k=k_val,label =labelno)

       





 




