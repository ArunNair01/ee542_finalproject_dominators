# copyright: TEAM_DOMINATORS
import pandas as pd 
import hashlib
import os 
import logging
import pdb
## Cancer types based on primarty sites

##No cancer - 0
##Kidney cancer - 1
##Thyroid cancer - 2
##Blood cancer - 3
##Uterus cancer - 4
##Head and Neck cancer - 5
##Ovary cancer - 6
##Breast cancer - 7
##Liver cancer - 8
##Esophagus cancer - 9
##Brain cancer - 10
##Cervix cancer - 11
##Lung cancer - 12
##Bladder cancer - 13
##Colorectal cancer - 14
##Stomach cancer - 15
##Eye cancer -16
##Pancreas cancer - 17
##Adrenal Gland cancer - 18
##Prostate cancer - 19
##Soft Tissue cancer - 20
##Testis cancer - 21
##Skin cancer - 22
##Bone Marrow cancer - 23
##Lymph Nodes cancer - 24
##Pleura cancer - 25
##Thymus cancer - 26
##Bile Duct cancer - 27




def extractLabel(inputfile):
    
            df = pd.read_csv(inputfile, sep="\t")
            #
            # print (df[columns])
            
            
            
            df['label'] = df['cases.0.project.primary_site']
            
            df.loc[df['cases.0.project.primary_site'].str.match("Kidney"), 'label'] = 1
            df.loc[df['cases.0.project.primary_site'].str.match("Thyroid"), 'label'] = 2
            df.loc[df['cases.0.project.primary_site'].str.match("Blood"), 'label'] = 3
            df.loc[df['cases.0.project.primary_site'].str.match("Uterus"), 'label'] = 4
            df.loc[df['cases.0.project.primary_site'].str.match("Head and Neck"), 'label'] = 5
            df.loc[df['cases.0.project.primary_site'].str.match("Ovary"), 'label'] = 6
            df.loc[df['cases.0.project.primary_site'].str.match("Breast"), 'label'] = 7
            df.loc[df['cases.0.project.primary_site'].str.match("Liver"), 'label'] = 8
            df.loc[df['cases.0.project.primary_site'].str.match("Esophagus"), 'label'] = 9
            df.loc[df['cases.0.project.primary_site'].str.match("Brain"), 'label'] = 10
            df.loc[df['cases.0.project.primary_site'].str.match("Cervix"), 'label'] = 11
            df.loc[df['cases.0.project.primary_site'].str.match("Lung"), 'label'] = 12
            df.loc[df['cases.0.project.primary_site'].str.match("Bladder"), 'label'] = 13
            df.loc[df['cases.0.project.primary_site'].str.match("Colorectal"), 'label'] = 14
            df.loc[df['cases.0.project.primary_site'].str.match("Stomach"), 'label'] = 15
            df.loc[df['cases.0.project.primary_site'].str.match("Eye"), 'label'] = 16
            df.loc[df['cases.0.project.primary_site'].str.match("Pancreas"), 'label'] = 17
            df.loc[df['cases.0.project.primary_site'].str.match("Adrenal"), 'label'] = 18
            df.loc[df['cases.0.project.primary_site'].str.match("Prostate"), 'label'] = 19
            df.loc[df['cases.0.project.primary_site'].str.match("Soft Tissue"), 'label'] = 20
            df.loc[df['cases.0.project.primary_site'].str.match("Testis"), 'label'] = 21
            df.loc[df['cases.0.project.primary_site'].str.match("Skin"), 'label'] = 22
            df.loc[df['cases.0.project.primary_site'].str.match("Bone Marrow"), 'label'] = 23
            df.loc[df['cases.0.project.primary_site'].str.match("Lymph Nodes"), 'label'] = 24
            df.loc[df['cases.0.project.primary_site'].str.match("Pleura"), 'label'] = 25
            df.loc[df['cases.0.project.primary_site'].str.match("Thymus"), 'label'] = 26
            df.loc[df['cases.0.project.primary_site'].str.match("Bile Duct"), 'label'] = 27
            df.loc[df['cases.0.samples.0.sample_type'].str.match("Solid Tissue Normal"), 'label'] = 0

            #Encode age
            df['age'] = df['cases.0.diagnoses.0.age_at_diagnosis']
            df.loc[(df['cases.0.diagnoses.0.age_at_diagnosis']>0) & (df['cases.0.diagnoses.0.age_at_diagnosis']<=10 ),'age']=1
            df.loc[(df['cases.0.diagnoses.0.age_at_diagnosis']>10) & (df['cases.0.diagnoses.0.age_at_diagnosis']<=20 ),'age']=2
            df.loc[(df['cases.0.diagnoses.0.age_at_diagnosis']>20) & (df['cases.0.diagnoses.0.age_at_diagnosis']<=30 ),'age']=3
            df.loc[(df['cases.0.diagnoses.0.age_at_diagnosis']>30) & (df['cases.0.diagnoses.0.age_at_diagnosis']<=40 ),'age']=4
            df.loc[(df['cases.0.diagnoses.0.age_at_diagnosis']>40) & (df['cases.0.diagnoses.0.age_at_diagnosis']<=50 ),'age']=5
            df.loc[(df['cases.0.diagnoses.0.age_at_diagnosis']>50) & (df['cases.0.diagnoses.0.age_at_diagnosis']<=60 ),'age']=6
            df.loc[(df['cases.0.diagnoses.0.age_at_diagnosis']>60) & (df['cases.0.diagnoses.0.age_at_diagnosis']<=70 ),'age']=7
            df.loc[(df['cases.0.diagnoses.0.age_at_diagnosis']>70) & (df['cases.0.diagnoses.0.age_at_diagnosis']<=80 ),'age']=8
            df.loc[(df['cases.0.diagnoses.0.age_at_diagnosis']>80) & (df['cases.0.diagnoses.0.age_at_diagnosis']<=90 ),'age']=9
            df.loc[(df['cases.0.diagnoses.0.age_at_diagnosis']>90) & (df['cases.0.diagnoses.0.age_at_diagnosis']<=100 ),'age']=10
            
            
            #Encode years smoked
            df['years_smoked']=df['cases.0.exposures.0.years_smoked']
            df.loc[(df['cases.0.exposures.0.years_smoked']>0) & (df['cases.0.exposures.0.years_smoked']<=10 ),'years_smoked']=1
            df.loc[(df['cases.0.exposures.0.years_smoked']>10) & (df['cases.0.exposures.0.years_smoked']<=20 ),'years_smoked']=2
            df.loc[(df['cases.0.exposures.0.years_smoked']>20) & (df['cases.0.exposures.0.years_smoked']<=30 ),'years_smoked']=3
            df.loc[(df['cases.0.exposures.0.years_smoked']>30) & (df['cases.0.exposures.0.years_smoked']<=40 ),'years_smoked']=4
            df.loc[(df['cases.0.exposures.0.years_smoked']>40) & (df['cases.0.exposures.0.years_smoked']<=50 ),'years_smoked']=5
            df.loc[(df['cases.0.exposures.0.years_smoked']>50) & (df['cases.0.exposures.0.years_smoked']<=60 ),'years_smoked']=6
            df.loc[(df['cases.0.exposures.0.years_smoked']>60) & (df['cases.0.exposures.0.years_smoked']<=70 ),'years_smoked']=7

            #Encode ethnicity 
            df['ethnicity']=df['cases.0.demographic.ethnicity']
            df.loc[df['cases.0.demographic.ethnicity'].str.match("hispanic or latino"), 'ethnicity'] = 1
            df.loc[df['cases.0.demographic.ethnicity'].str.match("not hispanic or latino"), 'ethnicity'] = 2
            df.loc[df['cases.0.demographic.ethnicity'].str.match("not reported"), 'ethnicity'] = 3
            df.loc[df['cases.0.demographic.ethnicity'].str.match("Unknown"), 'ethnicity'] = 4

            #Encode Race 
            df['race']=df['cases.0.demographic.race']
            df.loc[df['cases.0.demographic.race'].str.match("american indian or alaska native"), 'race'] = 1
            df.loc[df['cases.0.demographic.race'].str.match("asian"), 'race'] = 2
            df.loc[df['cases.0.demographic.race'].str.match("black or african american"), 'race'] = 3
            df.loc[df['cases.0.demographic.race'].str.match("native hawaiian or other pacific islander"), 'race'] = 4
            df.loc[df['cases.0.demographic.race'].str.match("not reported"), 'race'] = 5
            df.loc[df['cases.0.demographic.race'].str.match("other"), 'race'] = 6
            df.loc[df['cases.0.demographic.race'].str.match("white"), 'race'] = 7

            #Encode Gender 
            df['gender']=df['cases.0.demographic.gender']
            df.loc[df['cases.0.demographic.gender'].str.match("male"), 'gender'] = 1
            df.loc[df['cases.0.demographic.gender'].str.match("female"), 'gender'] = 2

            #Encode BMI 
            df['bmi']=df['cases.0.exposures.0.bmi']
            df.loc[(df['cases.0.exposures.0.bmi']>0) & (df['cases.0.exposures.0.bmi']<=20 ),'bmi']=1
            df.loc[(df['cases.0.exposures.0.bmi']>20) & (df['cases.0.exposures.0.bmi']<=30 ),'bmi']=2
            df.loc[(df['cases.0.exposures.0.bmi']>30) & (df['cases.0.exposures.0.bmi']<=40 ),'bmi']=3
            df.loc[(df['cases.0.exposures.0.bmi']>40),'bmi']=4

            #Encode number of cigarettes 
            df['cigarettes_consumed']=df['cases.0.exposures.0.cigarettes_per_day']
            df.loc[(df['cases.0.exposures.0.cigarettes_per_day']>0) & (df['cases.0.exposures.0.cigarettes_per_day']<=1 ),'cigarettes_consumed']=1
            df.loc[(df['cases.0.exposures.0.cigarettes_per_day']>1) & (df['cases.0.exposures.0.cigarettes_per_day']<=2 ),'cigarettes_consumed']=2
            df.loc[(df['cases.0.exposures.0.cigarettes_per_day']>2) & (df['cases.0.exposures.0.cigarettes_per_day']<=3 ),'cigarettes_consumed']=3
            df.loc[(df['cases.0.exposures.0.cigarettes_per_day']>3) & (df['cases.0.exposures.0.cigarettes_per_day']<=4 ),'cigarettes_consumed']=4
            df.loc[(df['cases.0.exposures.0.cigarettes_per_day']>4),'cigarettes_consumed']=5
            
            #primary_tumor_count = df.loc[df.label == 1].shape[0]
           
            #logging.info("{} Primary Tumor samples, {} Recurrent Tumor,{} Solid Tissue Normal,{} Peripheral Blood,{} Additional Metastatic,{} Primary Blood Derived Cancer - Bone Marrow,{} Recurrent Blood Derived Cancer - Bone Marrow,{} Recurrent Blood Derived Cancer - Peripheral Blood,{} Additional - New Primary,{} Cell Lines, {} Control Analyte, {} Metastatic  ".format(primary_tumor_count,recurrent_tumor_count,tissue_count,peripheral_blood_count,Additional_metastatic_count,Primary_Blood_Derived_Cancer_Bone_Marrow_count,Recurrent_Blood_Derived_Cancer_Bone_Marrow_count,Recurrent_Blood_Derived_Cancer_Peripheral_Blood_count,Additional_New_Primary_count,Cell_Lines_count,Control_Analyte_count,Metastatic_count))
            columns = ['file_id','age','years_smoked','ethnicity','race','gender','bmi','cigarettes_consumed','label']
            return df[columns]

if __name__ == '__main__':


        data_dir ="/Users/Shubham/Desktop/USC_Data/Sem3courses/EE542/final_project/"
        # Input directory and label file. The directory that holds the data. Modify this when use.
        
        label_file = data_dir + "files_meta.tsv"
        
        #output file
        outputfile = data_dir + "dominators_matrix.csv"

        # extract data
        label_df = extractLabel(label_file)
        
        input_half=data_dir+"half_matrix.csv"
        matrix_df=pd.read_csv(input_half)

        #merge the two based on the file_id
        result = pd.merge(matrix_df, label_df, on='file_id', how="left")
        #print(result)

        #save data
        result.to_csv(outputfile, index=False)
        #label_df.to_csv(outputfile, index=False)
        #print (labeldf)
