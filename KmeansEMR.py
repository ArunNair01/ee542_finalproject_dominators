from numpy import array
from math import sqrt
from pyspark import SparkConf, SparkContext
from pyspark.mllib.clustering import KMeans, KMeansModel
import sys

conf = (SparkConf().setAppName("dominators_final_project"))
sc = SparkContext(conf = conf)
# Load and parse the data
data = sc.textFile(sys.argv[1])
#print(data)

parsedData = data.map(lambda line: array([float(x) for x in line.split('\t')]))

# Build the model (cluster the data)
clusters = KMeans.train(parsedData, 8, maxIterations=50, initializationMode="random")

# Evaluate clustering by computing Within Set Sum of Squared Errors
def error(point):
    center = clusters.centers[clusters.predict(point)]
    return sqrt(sum([x**2 for x in (point - center)]))

WSSSE = parsedData.map(lambda point: error(point)).reduce(lambda x, y: x + y)
print("Within Set Sum of Squared Error = " + str(WSSSE))

clusters_label = parsedData.map(lambda point:clusters.predict(point))
print("cluster labels are")
print(clusters_label.collect())
#clusters_label.count()
#print clusters
# Save and load model
#clusters.save(sc,sys.argv[2] )
#sameModel = KMeansModel.load(sc, "target/org/apache/spark/PythonKMeansExample/KMeansModel")
#clusters.saveAsTextFile(sys.argv[2])

